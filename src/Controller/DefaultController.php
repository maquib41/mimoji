<?php

namespace App\Controller;

use Pimcore\Controller\FrontendController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Services\OpenAIConnectorService;

class DefaultController extends FrontendController
{
    /**
     * @param Request $request
     * @return Response
     */
    public function defaultAction(Request $request): Response
    {
        return $this->render('default/default.html.twig');
    }

    /**
     * @Route("/name")
     */
    public function openApiDetails(Request $request,OpenAIConnectorService $OpenAIConnectorService){
        $open_ai_key = $this->getParameter('app.custom_param');
        $prompt = 'Say this is a test';
        $dataRequest = [
            'model' => 'text-davinci-003',
            'prompt' => $prompt,
            'temperature' => 0,
            'max_tokens' => 7,
            'frequency_penalty' => 0,
            'presence_penalty' => 0.6,

        ];
        $Sevice = $OpenAIConnectorService->getOpenApi($open_ai_key,$dataRequest);
    }
}
