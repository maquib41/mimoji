<?php

return [
    "bundle" => [
        "Pimcore\\Bundle\\DataImporterBundle\\PimcoreDataImporterBundle" => TRUE,
        "Pimcore\\Bundle\\DataHubBundle\\PimcoreDataHubBundle" => [
            "enabled" => TRUE,
            "priority" => 20,
            "environments" => [

            ]
        ],
        "Pimcore\\Bundle\\BundleGeneratorBundle\\PimcoreBundleGeneratorBundle" => TRUE,
        "MimojiBundle\\MimojiBundle" => TRUE
    ]
];
