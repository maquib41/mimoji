pimcore.registerNS("pimcore.plugin.MimojiBundle");

pimcore.plugin.MimojiBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.MimojiBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("MimojiBundle ready!");
    }
});

var MimojiBundlePlugin = new pimcore.plugin.MimojiBundle();
