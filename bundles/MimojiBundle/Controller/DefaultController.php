<?php

namespace MimojiBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use MimojiBundle\Services\OpenAIConnector;
use Psr\Log\LoggerInterface;

class DefaultController extends FrontendController
{
    /**
     * @var LoggerInterface
     */
    private $mimojiLogger;

    public function __construct(LoggerInterface $mimojiLogger)
    {
        $this->mimojiLogger = $mimojiLogger;
    }


    /**
     * @Route("/mimoji")
     */
    public function indexAction(Request $request)
    {
        return new Response('Hello world from mimoji');
    }

    /**
     * @Route("/connector")
     */
    public function openApiDetails(Request $request,OpenAIConnector $OpenAIConnector){
        $open_ai_key = $this->getParameter('app.custom_param');
        $prompt = 'Say this is a test';
        if($prompt != null){
            $dataRequest = [
                'model' => 'text-davinci-003',
                'prompt' => $prompt,
            ];
            $Sevice = $OpenAIConnector->getOpenApi($open_ai_key,$dataRequest);
        }
        else{
            $info = $this->mimojiLogger->error('Prompt should not be null.');
            return $this->json($open_ai_key);
        }
        
        //$info = $this->mimojiLogger->info('Tony Vairelles\' hairdresser.');
        
    }
}
