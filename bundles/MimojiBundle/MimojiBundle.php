<?php

namespace MimojiBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class MimojiBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/mimoji/js/pimcore/startup.js'
        ];
    }
}